#!/bin/sh

# SPDX-FileCopyrightText: 2022 OpenRA Ladder Project
#
# SPDX-License-Identifier: GPL-3.0-or-later

Name="${Name:-"OpenRA Red Alert DevTest 20220910 Server"}"
Mod="${Mod:-"ra"}"
ListenPort="${ListenPort:-"1234"}"
AdvertiseOnline="${AdvertiseOnline:-"True"}"
Password="${Password:-""}"
RecordReplays="${RecordReplays:-"True"}"

RequireAuthentication="${RequireAuthentication:-"False"}"
ProfileIDBlacklist="${ProfileIDBlacklist:-""}"
ProfileIDWhitelist="${ProfileIDWhitelist:-""}"

EnableSingleplayer="${EnableSingleplayer:-"False"}"
EnableSyncReports="${EnableSyncReports:-"False"}"
EnableGeoIP="${EnableGeoIP:-"True"}"
EnableLintChecks="${EnableLintChecks:-"True"}"
ShareAnonymizedIPs="${ShareAnonymizedIPs:-"True"}"

JoinChatDelay="${JoinChatDelay:-"5000"}"

SupportDir="${SupportDir:-""}"

/home/openra/app/bin/openra-${Mod:-"ra"}-server \
     Server.Name="$Name" \
     Server.ListenPort="$ListenPort" \
     Server.AdvertiseOnline="$AdvertiseOnline" \
     Server.EnableSingleplayer="$EnableSingleplayer" \
     Server.Password="$Password" \
     Server.RecordReplays="$RecordReplays" \
     Server.RequireAuthentication="$RequireAuthentication" \
     Server.ProfileIDBlacklist="$ProfileIDBlacklist" \
     Server.ProfileIDWhitelist="$ProfileIDWhitelist" \
     Server.EnableSyncReports="$EnableSyncReports" \
     Server.EnableGeoIP="$EnableGeoIP" \
     Server.EnableLintChecks="$EnableLintChecks" \
     Server.ShareAnonymizedIPs="$ShareAnonymizedIPs" \
     Server.JoinChatDelay="$JoinChatDelay" \
     Engine.SupportDir="$SupportDir"