# SPDX-FileCopyrightText: 2022 OpenRA Ladder Project
#
# SPDX-License-Identifier: GPL-3.0-or-later

FROM mono

RUN useradd -m openra

WORKDIR /home/openra

ADD app/usr ./app
ADD entrypoint.sh ./

RUN chown openra: -R /home/openra \
    && chmod +x entrypoint.sh

RUN mkdir -p /data/openra && cd /data/openra && mkdir Logs Replays maps && chown openra: -R /data/openra

ENV Mod="ra"
ENV SupportDir="/data/openra"

USER openra

EXPOSE 1234

ENTRYPOINT ["/home/openra/entrypoint.sh"]